import React from 'react';
import './App.css';
import './i18n'
import { useTranslation } from 'react-i18next';

function App() {
	const { t } = useTranslation()

	return (
		<div className="App">
			<header className="App-header">
				{t('Welcome to Seeds')}
			</header>
		</div>
	);
}

export default App;
